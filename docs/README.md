---
sidebar: false
title: PlantingSpace
description: We build tools that aim to evolve the patterns of how humans interact, discover, and create.
features:
- title: Taiga
  details: 
      The aim of Taiga is to create a tool for gathering knowledge and gaining insights. This should help us to find the frontiers of what is known, and help us push beyond that. Implementation draws heavily on domains of category theory and probabilistic modeling.
- title: Mangrove
  link: https://open-reviews.net
  details: |
    Mangrove is an initiative to create a public space on the Internet where people can freely share reviews with each other and make better decisions based on this open data. An infrastructure for reviews on which anyone can build, without barriers to entry.
- title: Broadleaf
  link: broadleaf.html
  details: |
    Broadleaf reimagines how people work together towards achieving a goal. It is an open standard, built from first principles, that describes values, provides concrete specifications, and the tools for organizing work effectively.
---

<Home />

# Get in touch
We welcome your involvement and input! Join discussion now on [Element](https://app.element.io/#/room/#plantingspace:matrix.org) or email us at hello@planting.space

## Follow us

Stay tuned on [Twitter](https://twitter.com/PlantingSpace), [Mastodon](https://mas.to/@PlantingSpace), [RSS](https://mas.to/users/PlantingSpace.rss) or mailing list:
  


<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
</style>
<div id="mc_embed_signup">
<form action="https://space.us20.list-manage.com/subscribe/post?u=384a3d5b51e32c2fcbab0ebd2&amp;id=1059c0fb6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_384a3d5b51e32c2fcbab0ebd2_1059c0fb6e" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->

<br>

<div style="text-align: center;">
<iframe allowfullscreen sandbox="allow-top-navigation allow-scripts" frameBorder="0" src="https://www.mastofeed.com/apiv2/feed?userurl=https%3A%2F%2Fmas.to%2Fusers%2FPlantingSpace&theme=light&size=100&header=false&replies=false&boosts=false" width="400" height="1500" />
<br>
Retrieved from <a rel="me" href="https://mas.to/@PlantingSpace">Mastodon</a>
</div>

# Legal entity

PlantingSpace is a limited liability company (GmbH) [registered](https://zg.chregister.ch/cr-portal/auszug/auszug.xhtml?uid=CHE-469.094.556#) in Zug, Switzerland.
